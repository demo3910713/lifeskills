#  Framework #
 A framework is like a structure that provides a base for the application development process. It is unique, With the help of a framework, you can avoid writing everything from scratch. Frameworks provide a set of tools and elements that help in the speedy development process.

 <center><img src='https://media.geeksforgeeks.org/wp-content/uploads/20230821163240/What-is-Framework.png' width='400' height='200'></center>

# Frontend frameworks #
Front-end frameworks are collection of packages prewritten codes and help developers quickly and easily create the user interface of a website or web application.
### Popular Frontend frameworks : ### 
* React
* Angular
* Vue.js

## React ## 
<center> <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/React_Logo_SVG.svg/1024px-React_Logo_SVG.svg.png' width='400' height='200'></center>

  1. React was Discovered by 'Jordan Walke', intial releae at May 29, 2013  and stable release 14 June 2022.
  1. React is a free and open source forntend for building user interfaces based on components. It is maintained by Meta (formerly Facebook) community of individual developers and companies.
  1. Main reason for React's popularity is its ease of use.
  1. By using this framework developers can make easy to build huge components and reuse the strucutre and saves time and reduces the risk of errors
## Angular ##
<center><img src='https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/800px-Angular_full_color_logo.svg.png' width='300' height='200'></center>

1. Angular was discovered by 'Google' intial release at 14 septemeber 2016, stable release 13 march 2024
1. Since v9, the Angular team has moved all new applications to use the Ivy compiler and runtime. They will be working on Ivy to improve output bundle sizes and development speeds.
1. This framework is also incredibly reliable and simple for someone with a solid background. 
##  Vue.js ##
<center><img src='https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/1024px-Vue.js_Logo_2.svg.png' width='300' height='200'></center>

1. Vue.js was discovered by 'Evan You' intial release at Fed 2014.
1. It is an open-source model–view–viewmodel front end JavaScript library for building user interfaces and single-page applications
1. Vue.js features an incrementally adaptable architecture that focuses on declarative rendering and component composition.
## References ##
https://www.wikipedia.org/
