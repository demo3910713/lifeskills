# What are the steps/strategies to do Active Listening?
1. Take notes if necessary.
2. Don't get distracted
3. Paraphrase to make sure that both of you are on the same page.
5. Don't interrupt. Let the speaker complete himself.
6. Show that you are listening with body language.

# According to Fisher's model, what are the key points of Reflective Listening? 
1. This approach concentrates on attentive listening with a focus on reflection.
2. It also involves reflecting empathy. It is believed that doing so will help get involved in the conversation more and more.
3. Regularly rephrasing the speaker's statements is recommended to ensure a shared understanding and alignment.
4. Maintaining a receptive mindset and sharing personal opinions more confident and comprehensive expression from the speaker.

# What are the obstacles in your listening process?
Obstacles to my listening process may include 
1. distractions.
2. preconceived judgments.
3. Tendency to formulate responses while the speaker is talking.
# What can you do to improve your listening?
To improve listening. 
1. I can practice mindfulness.
2. Minimize distractions.
3. Consciously focus on the speaker.

# When do you switch to Passive communication style in your day to day life?
1. Passive communication can happen due to lack of concentration.
2. Fear of losing the argument.
3. Fear of conflict.
   
# When do you switch into Aggressive communication styles in your day to day life?
1. Aggressive communication can happen when you want to dominate the conversation. 
1. Raise your voice out of anger.
1. when we feel threatened.
1. At time of getting  successful
# When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
1. When the other person lacks accountability
1. When person is not ready to admit to his wrongdoings.
# How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? 
1. Recognize and name your feelings.
1. Recognize and name what you need.
1. Discussing the thoughts with others.
1. Be aware of situations.














